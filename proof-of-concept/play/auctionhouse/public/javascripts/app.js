angular.module('auction', []);

angular.module('auction').controller('AuctionController', ['$scope', '$http', function($scope, $http) {


    $scope.items = [];
    $http.get("/item").success(function(items) {
        $scope.items = items;

    });

    var socket = new ReconnectingWebSocket(ws_url("socket"));

    socket.onopen = function(){
        console.log("Socket has been opened!");
    };
    socket.onmessage = function(message) {
        console.log(JSON.parse(message.data));
        listener(JSON.parse(message.data));
    };

    function listener(data) {
        $scope.$apply(
            $scope.items.push(data)
        )

    }

    function ws_url(s) {
          var l = window.location;
          var r = ((l.protocol === "https:") ? "wss://" : "ws://") + l.hostname + (((l.port != 80) && (l.port != 443)) ? ":" + l.port : "") + l.pathname + s;
          //console.log(r);
          return r;
      }


}]);

angular.element(document).ready(function() {
    angular.bootstrap(document, ['auction']);
});