name := "auctionhouse"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  jdbc,
  "com.typesafe.play" % "play-slick_2.10" % "0.6.0.1",
  "mysql" % "mysql-connector-java" % "5.1.27",
  "com.google.inject" % "guice" % "3.0",
  "javax.inject" % "javax.inject" % "1",
  "com.github.tototoshi" %% "slick-joda-mapper" % "1.0.1"
)     

play.Project.playScalaSettings
