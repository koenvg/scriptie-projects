package Actor

import play.api.libs.json._
import play.api.libs.json.Json._

import akka.actor.Actor

import play.api.libs.iteratee.{Concurrent, Enumerator}

import play.api.libs.iteratee.Concurrent.Channel
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits._

import scala.concurrent.duration._
import models.Item
import scala.collection.mutable.ArrayBuffer
import java.util.UUID


class AuctionActor extends Actor{

  case class UserChannel(id: UUID, enumerator: Enumerator[JsValue], channel: Channel[JsValue])

  var webSockets = Map[UUID, UserChannel]()
  override def receive = {
    case StartSocket(id) =>

      val broadcast: (Enumerator[JsValue], Channel[JsValue]) = Concurrent.broadcast[JsValue]
      val userSocket = UserChannel(id, broadcast._1, broadcast._2)
      webSockets += (id -> userSocket)
      sender ! userSocket.enumerator

    case SocketClosed(id) =>
      val userChannel = webSockets.get(id).get
      webSockets -= userChannel.id


    case UpdateInfo(item) =>

      webSockets.foreach(map => {
        val channel = map._2.channel
        channel.push(item.toJson)
      })
  }
}

sealed trait SocketMessage

case class StartSocket(id: UUID) extends SocketMessage
case class SocketClosed(id: UUID) extends SocketMessage
case class UpdateInfo(item: Item) extends SocketMessage
