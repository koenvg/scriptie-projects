package entities

import play.api.db.slick.Config.driver.simple._
import models.Offer
import scala.slick.model.ForeignKeyAction

class Offers(tag: Tag) extends EntityModel[Offer](tag, "offers") {

  val price: Column[BigDecimal] = column[BigDecimal]("price")
  val itemId: Column[Int] = column[Int]("item_id")
  val contactEmail: Column[String] = column[String]("contact_email")

  val items = TableQuery[Items]

  val item_offers = foreignKey("FK_item_offers", itemId, items)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

  def * = (id.?, price, itemId, contactEmail) <> (Offer.tupled, Offer.unapply)
}
