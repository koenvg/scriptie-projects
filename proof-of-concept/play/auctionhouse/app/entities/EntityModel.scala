package entities

import play.api.db.slick.Config.driver.simple._
import models.BaseModel

abstract class EntityModel[M <: BaseModel](tag: Tag, tableName: String) extends Table[M](tag, tableName){

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
}
