package entities

import play.api.db.slick.Config.driver.simple._
import models.Item
import com.github.tototoshi.slick.MySQLJodaSupport._
import org.joda.time.DateTime

class Items(tag: Tag) extends EntityModel[Item](tag, "items"){

  val name: Column[String] = column[String]("name")
  val description: Column[String] = column[String]("description")
  val startPrice: Column[BigDecimal] = column[BigDecimal]("start_price")
  val buyOutPrice: Column[BigDecimal] = column[BigDecimal]("buy_out_price")
  val contactEmail: Column[String] = column[String]("contact_email")
  val creationDate: Column[DateTime] = column[DateTime]("creation_date")

  def * = (id.?, name, description, startPrice, buyOutPrice, contactEmail, creationDate) <> (Item.tupled, Item.unapply)
}
