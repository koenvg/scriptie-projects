package models

case class Offer(id: Option[Int],
                  price: BigDecimal,
                  itemId: Int,
                  contactEmail: String) extends BaseModel{

}
