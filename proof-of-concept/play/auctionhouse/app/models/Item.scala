package models

import repositories.dbAccess
import play.api.db.slick.Config.driver.simple._
import entities.Offers
import org.joda.time.DateTime
import play.api.libs.json.{JsValue, Json}
import org.joda.time.format.ISODateTimeFormat

case class Item(id: Option[Int],
                 name: String,
                 description: String,
                 startPrice: BigDecimal,
                 buyOutPrice: BigDecimal,
                 contactEmail: String,
                 creationDate: DateTime) extends BaseModel with dbAccess{


  lazy val offerTable = TableQuery[Offers]

  lazy val offers : List[Offer] = dbOperation { implicit session: Session =>
     offerTable.where(offer => offer.itemId === id).map(m => m).list()
  }

  def toJson : JsValue  = {
    val formatter = ISODateTimeFormat.dateTime()

    val json = Json.obj(
      "id" -> id,
      "name" -> name,
      "description" -> description,
      "startPrice" -> startPrice,
      "buyOutPrice" -> buyOutPrice,
      "contactEmail" -> contactEmail,
      "creationDate" -> formatter.print(creationDate)
    )
    return json
  }

}
