package models

trait BaseModel {
  val id: Option[Int]
}
