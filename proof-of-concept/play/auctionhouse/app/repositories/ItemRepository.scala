package repositories

import models.Item
import entities.Items
import play.api.db.slick.Config.driver.simple._

object ItemRepository extends BaseRepositoryImpl[Item, Items]{
  override protected def table: TableQuery[Items] = TableQuery[Items]
}
