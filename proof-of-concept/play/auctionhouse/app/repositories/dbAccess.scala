package repositories

import play.api.db.slick.Config.driver.simple._

import play.api.db.DB
import play.api.Play.current
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global

trait dbAccess {

  implicit lazy val db = Database.forDataSource(DB.getDataSource())

  def dbAsync[T](block: Session => T): Future[T] = {
    Future { db.withSession { block }}
  }

  def dbOperation[T](block: Session => T) : T = {
    db.withSession { block }
  }
}

