package repositories

import play.api.db.slick.Config.driver.simple._
import models.BaseModel


trait BaseRepository[M <: BaseModel] {

  def get(id: Int)(implicit session: Session): Option[M]
  def all()(implicit session: Session): List[M]
  def count(implicit session: Session): Int
  def page(page: Int, size: Int)(implicit session: Session): List[M]
  def save(model: M)(implicit session: Session): M
  def delete(model: M)(implicit session: Session)

}
