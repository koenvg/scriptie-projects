package repositories

import play.api.db.slick.Config.driver.simple._
import entities.EntityModel
import models.BaseModel


trait BaseRepositoryImpl[M <: BaseModel, T <: EntityModel[M]] extends BaseRepository[M] {

  protected def table: TableQuery[T]

  def all()(implicit session: Session): List[M] = {
    table.list()
  }

  def page(page: Int = 0, size: Int = 20)(implicit session: Session): List[M] = {
    table.sortBy(t => t.id.desc).drop(page * size).take(size).list()
  }

  def count(implicit session: Session): Int = {
    table.length.run
  }

  def get(id: Int)(implicit session: Session): Option[M] = {
    val test = table;
    test.where(t => t.id === id).map(t => t).firstOption
  }

  def save(model: M)(implicit session: Session): M = {
    model.id match {
      case Some(id) => update(model)
      case None => insert(model)
    }
    model
  }

  def delete(model: M)(implicit session: Session) = {
    table.where(t => t.id === model.id).delete
  }

  def insert(model: M)(implicit session: Session): M = {
    val id = (table returning table.map(_.id)) += model
    get(id).get
  }

  private def update(model: M)(implicit session: Session): M = {
    table.where(t => t.id === model.id).update(model)
    model
  }

}
