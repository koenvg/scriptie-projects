package repositories

import play.api.db.slick.Config.driver.simple._
import models.Offer
import entities.Offers

object OfferRepository extends BaseRepositoryImpl[Offer, Offers]{
  override protected def table: TableQuery[Offers] = TableQuery[Offers]
}
