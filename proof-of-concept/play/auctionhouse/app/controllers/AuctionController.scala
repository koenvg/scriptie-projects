package controllers

import play.api.mvc._
import play.api.libs.json._
import play.api.libs.concurrent._
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.iteratee.{Enumerator, Iteratee}

import scala.concurrent.duration._
import akka.actor.Props
import akka.pattern.ask
import akka.util.Timeout
import Actor.{UpdateInfo, SocketClosed, StartSocket, AuctionActor}
import play.api.data.Forms._
import anorm._
import play.api.data._
import models.Item
import org.joda.time.DateTime
import views.forms.ItemForm
import services.AuctionService
import play.api.Routes


object AuctionController extends Controller with Secured{

  val auctionActor = Akka.system.actorOf(Props[AuctionActor])

  def index() = withAuth { implicit request => userUd =>
    Ok(views.html.index("hallo"))
  }

  def get(id: Int) = TODO


  val itemForm = Form(
    mapping(
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "startPrice" -> bigDecimal,
      "buyoutPrice" -> bigDecimal,
      "contactEmail" -> nonEmptyText
    )(ItemForm.apply)(ItemForm.unapply)
  )
  def newItem() = withAuth { implicit request => userId =>
    Ok(views.html.addItem(itemForm))
  }

  def createItem() = Action { implicit request =>
    itemForm.bindFromRequest.fold(
      formWithErrors => BadRequest(""),
      formItem => {

        val item = AuctionService.createItem(formItem)

        item.onSuccess{
          case item => auctionActor ! UpdateInfo(item)
        }
        Redirect("/")
      }
    )
  }


  def delete(id: Int) = TODO

  def all() = Action.async { implicit request =>
    AuctionService.all().map(items => Ok(JsArray(items.map(item => item.toJson))))
  }

  def webSocket() = withAuthWS { userId =>
    implicit val timeout = Timeout(3 seconds)

    (auctionActor ? StartSocket(userId)) map {
      enumerator =>

        // create a Itreatee which ignore the input and
        // and send a SocketClosed message to the actor when
        // connection is closed from the client
        (Iteratee.ignore[JsValue] map {
          _ =>
            auctionActor ! SocketClosed(userId)
        }, enumerator.asInstanceOf[Enumerator[JsValue]])
    }
  }
}
