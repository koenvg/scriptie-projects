package services

import scala.concurrent.Future
import models.Item
import repositories.{dbAccess, ItemRepository}
import views.forms.ItemForm
import org.joda.time.DateTime



object AuctionService extends dbAccess{

  def all(): Future[List[Item]] = dbAsync { implicit session =>
    ItemRepository.all()
  }

  def get(id: Int): Future[Option[Item]] = dbAsync { implicit session =>
    ItemRepository.get(id)
  }

  def createItem(formItem: ItemForm): Future[Item] = dbAsync{ implicit session =>
    val item = Item(None, formItem.name, formItem.description, formItem.startPrice, formItem.buyOutPrice, formItem.contactEmail, DateTime.now())
    ItemRepository.save(item)
  }
}
