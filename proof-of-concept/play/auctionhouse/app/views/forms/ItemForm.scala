package views.forms

/**
 * Created by koen on 28.05.14.
 */
case class ItemForm(name: String,
                    description: String,
                    startPrice: BigDecimal,
                    buyOutPrice: BigDecimal,
                    contactEmail: String) {

}
