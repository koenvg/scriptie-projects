# --- Created by Slick DDL
# To stop Slick DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table `items` (`id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,`name` VARCHAR(254) NOT NULL,`description` VARCHAR(254) NOT NULL,`start_price` DECIMAL(21,2) NOT NULL,`buy_out_price` DECIMAL(21,2) NOT NULL,`contact_email` VARCHAR(254) NOT NULL,`creation_date` TIMESTAMP NOT NULL);
create table `offers` (`id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,`price` DECIMAL(21,2) NOT NULL,`item_id` INTEGER NOT NULL,`contact_email` VARCHAR(254) NOT NULL);
alter table `offers` add constraint `FK_item_offers` foreign key(`item_id`) references `items`(`id`) on update NO ACTION on delete NO ACTION;

# --- !Downs

ALTER TABLE offers DROP FOREIGN KEY FK_item_offers;
drop table `items`;
drop table `offers`;

