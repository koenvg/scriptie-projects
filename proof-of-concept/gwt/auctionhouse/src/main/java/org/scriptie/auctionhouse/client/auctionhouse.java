package org.scriptie.auctionhouse.client;


import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class auctionhouse implements EntryPoint {
    private static final int REFRESH_INTERVAL = 5000; // ms
    private VerticalPanel mainPanel = new VerticalPanel();
    private FlexTable stocksFlexTable = new FlexTable();
    private Label lastUpdatedLabel = new Label();
    private ItemServiceAsync itemService = GWT.create(ItemService.class);

    /**  * Entry point method.  */
    public void onModuleLoad() {
        // Create table for stock data
        stocksFlexTable.setText(0, 0, "Symbol");
        stocksFlexTable.setText(0, 1, "Price");
        stocksFlexTable.setText(0, 2, "Change");
        stocksFlexTable.setText(0, 3, "Remove");

        // Add styles to elements in the stock list table.
        stocksFlexTable.getRowFormatter().addStyleName(0, "watchListHeader");
        stocksFlexTable.addStyleName("watchList");
        stocksFlexTable.getCellFormatter().addStyleName(0, 1, "watchListNumericColumn");
        stocksFlexTable.getCellFormatter().addStyleName(0, 2, "watchListNumericColumn");
        stocksFlexTable.getCellFormatter().addStyleName(0, 3, "watchListRemoveColumn");
        stocksFlexTable.setCellPadding(6);


        // Assemble Main panel.
        mainPanel.add(stocksFlexTable);
        mainPanel.add(lastUpdatedLabel);

        // Associate the Main panel with the HTML host page.
        RootPanel.get("itemList").add(mainPanel);

        // Setup timer to refresh list automatically.
        Timer refreshTimer = new Timer() {
            @Override
            public void run() {
                updateTable();
            }
        };
        refreshTimer.scheduleRepeating(REFRESH_INTERVAL);

        createForm();
    }

    private void updateTable() {

        final AsyncCallback<List<ItemDto>> callback = new AsyncCallback<List<ItemDto>>() {
            public void onFailure(Throwable caught) {
                lastUpdatedLabel.setText(caught.getMessage());
            }

            public void onSuccess(List<ItemDto> result) {
                lastUpdatedLabel.setText(DateTimeFormat.getMediumDateTimeFormat().format(new Date()));
            }
        };

        itemService.all(callback);
    }

    private void createForm(){
// Create a FormPanel and point it at a service.
        final FormPanel form = new FormPanel();
        form.setAction("/myFormHandler");

        // Because we're going to add a FileUpload widget, we'll need to set the
        // form to use the POST method, and multipart MIME encoding.
        form.setEncoding(FormPanel.ENCODING_MULTIPART);
        form.setMethod(FormPanel.METHOD_POST);

        // Create a panel to hold all of the form widgets.
        Grid grid = new Grid(6, 2);
        form.setWidget(grid);

        // Create a TextBox, giving it a name so that it will be submitted.
        final TextBox txtName = new TextBox();
        final Label lblName = new Label("name");

        final TextArea txaDescription = new TextArea();
        final Label lblDescription = new Label("Description");

        final TextBox txtStartPrice = new TextBox();
        final Label lblStartPrice = new Label("Start price");

        final TextBox txtBuyOutPrice = new TextBox();
        final Label lblBuyOutPrice = new Label("Buy out price");

        final TextBox txtContactEmail = new TextBox();
        final Label lblContactEmail = new Label("email");

        Button btnSubmit = new Button("Submit", new ClickHandler() {
            public void onClick(ClickEvent event) {
                form.submit();
            }
        });

        grid.setWidget(0,0,lblName);
        grid.setWidget(0,1,txtName);
        grid.setWidget(1,0,lblDescription);
        grid.setWidget(1,1,txaDescription);
        grid.setWidget(2,0,lblStartPrice);
        grid.setWidget(2,1,txtStartPrice);
        grid.setWidget(3,0,lblBuyOutPrice);
        grid.setWidget(3,1,txtBuyOutPrice);
        grid.setWidget(4,0,lblContactEmail);
        grid.setWidget(4,1,txtContactEmail);
        grid.setWidget(5,0,btnSubmit);

        // Add a 'submit' button.

        final AsyncCallback<ItemDto> callback = new AsyncCallback<ItemDto>() {
            public void onFailure(Throwable caught) {
                // TODO: Do something with errors.
            }

            public void onSuccess(ItemDto result) {
                updateTable();
            }
        };
        form.addSubmitHandler(new FormPanel.SubmitHandler() {
            public void onSubmit(FormPanel.SubmitEvent event) {
                // This event is fired just before the form is submitted. We can take
                // this opportunity to perform validation.
                itemService.create(txtName.getText(), txaDescription.getText(), BigInteger.valueOf(Long.parseLong(txtStartPrice.getText())), BigInteger.valueOf(Long.parseLong(txtBuyOutPrice.getText())), txtContactEmail.getText(), callback);
            }
        });
        form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
            public void onSubmitComplete(FormPanel.SubmitCompleteEvent event) {
                // When the form submission is successfully completed, this event is
                // fired. Assuming the service returned a response of type text/html,
                // we can get the result text here (see the FormPanel documentation for
                // further explanation).
                Window.alert(event.getResults());
            }
        });


        RootPanel.get("itemForm").add(form);
    }
}
