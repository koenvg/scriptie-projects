package org.scriptie.auctionhouse.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.scriptie.auctionhouse.client.ItemService;
import org.scriptie.auctionhouse.domain.Item;
import org.scriptie.auctionhouse.client.ItemDto;
import org.scriptie.auctionhouse.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by koen on 30.05.14.
 */
public class ItemServiceImpl  extends RemoteServiceServlet implements
        ItemService {

    @Autowired
    private ItemRepository itemRepository;

    public ItemServiceImpl() {
    }

    public List<ItemDto> all() {
        List<Item> items = itemRepository.findAll();
        List<ItemDto> itemDtos = new ArrayList<ItemDto>();
        for(Item item: items) {
            itemDtos.add(new ItemDto(item.getId(), item.getName(), item.getDescription(), item.getStartPrice(), item.getBuyOutPrice(), item.getContactEmail(), item.getCreationDate().getTime()));
        }

        return itemDtos;
    }

    public ItemDto create(String name, String description, BigInteger startPrice, BigInteger buyOutPrice, String contactEmail) {
        Item saveItem = new Item(name,description, startPrice, buyOutPrice, contactEmail, Calendar.getInstance());
        Item item = itemRepository.saveAndFlush(saveItem);
        return new ItemDto(item.getId(), item.getName(), item.getDescription(), item.getStartPrice(), item.getBuyOutPrice(), item.getContactEmail(), item.getCreationDate().getTime());
    }

    public ItemDto get(int id) {
        Item item = itemRepository.findOne(Integer.toString(id));
        return new ItemDto(item.getId(), item.getName(), item.getDescription(), item.getStartPrice(), item.getBuyOutPrice(), item.getContactEmail(), item.getCreationDate().getTime());
    }
}
