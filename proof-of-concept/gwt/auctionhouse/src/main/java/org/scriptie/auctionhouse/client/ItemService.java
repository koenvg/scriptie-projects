package org.scriptie.auctionhouse.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.math.BigInteger;
import java.util.List;


@RemoteServiceRelativePath("item")
public interface ItemService extends RemoteService {
    List<ItemDto> all();
    ItemDto create(String name, String description, BigInteger startPrice, BigInteger buyOutPrice, String contactEmail);
    ItemDto get(int id);
}
