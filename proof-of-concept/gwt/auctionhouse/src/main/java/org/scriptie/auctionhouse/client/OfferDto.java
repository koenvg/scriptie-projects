package org.scriptie.auctionhouse.client;

import java.math.BigInteger;

/**
 * Created by koen on 30.05.14.
 */
public class OfferDto {
    private int Id;
    private BigInteger price;

    public OfferDto(){

    }
    public OfferDto(int id, BigInteger price) {
        Id = id;
        this.price = price;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public BigInteger getPrice() {
        return price;
    }

    public void setPrice(BigInteger price) {
        this.price = price;
    }
}
