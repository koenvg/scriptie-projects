package org.scriptie.auctionhouse.domain;


import org.scriptie.auctionhouse.client.OfferDto;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by koen on 29.05.14.
 */
public class Offer implements Serializable{

    private int id;
    private BigInteger price;

    public Offer(int id, BigInteger price) {
        this.id = id;
        this.price = price;
    }

    public Offer(OfferDto offerDto) {
        this.id = offerDto.getId();
        this.price = offerDto.getPrice();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigInteger getPrice() {
        return price;
    }

    public void setPrice(BigInteger price) {
        this.price = price;
    }
}
