package org.scriptie.auctionhouse.domain;




import org.scriptie.auctionhouse.client.ItemDto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "item")
public class Item implements Serializable{
    @Id
    private int id;
    private String name;
    private String description;
    private BigInteger startPrice;
    private BigInteger buyOutPrice;
    private String contactEmail;
    private Date creationDate;

    private List<Offer> offers;

    public Item(String name, String description, BigInteger startPrice, BigInteger buyOutPrice, String contactEmail, Date creationDate) {

        this.name = name;
        this.description = description;
        this.startPrice = startPrice;
        this.buyOutPrice = buyOutPrice;
        this.contactEmail = contactEmail;
        this.creationDate = creationDate;
    }

    public Item(ItemDto itemDto) {
        this.id = itemDto.getId();
        this.name = itemDto.getName();
        this.description = itemDto.getDescription();
        this.startPrice = itemDto.getStartPrice();
        this.buyOutPrice = itemDto.getBuyOutPrice();
        this.contactEmail = itemDto.getContactEmail();
        this.creationDate = itemDto.getCreationDate();
        //this.offers = itemDto.getOffers();
    }

    public void addOffer(Offer offer) {
        if(offer == null){
            offers = new ArrayList<Offer>();
        }

        offers.add(offer);
    }

    public Offer getOffer(int id) {
        for(Offer offer: offers) {
            if(offer.getId() == id) {
                return offer;
            }
        }
        return null;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigInteger getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigInteger startPrice) {
        this.startPrice = startPrice;
    }

    public BigInteger getBuyOutPrice() {
        return buyOutPrice;
    }

    public void setBuyOutPrice(BigInteger buyOutPrice) {
        this.buyOutPrice = buyOutPrice;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
