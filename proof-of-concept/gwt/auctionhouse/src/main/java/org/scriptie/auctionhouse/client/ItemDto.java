package org.scriptie.auctionhouse.client;



import org.scriptie.auctionhouse.domain.Item;
import org.scriptie.auctionhouse.domain.Offer;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by koen on 30.05.14.
 */
public class ItemDto implements Serializable {

    private int id;
    private String name;
    private String description;
    private BigInteger startPrice;
    private BigInteger buyOutPrice;
    private String contactEmail;
    private Date creationDate;

    private List<OfferDto> offers;

    public ItemDto(){

    }
    public ItemDto(int id, String name, String description, BigInteger startPrice, BigInteger buyOutPrice, String contactEmail, Date creationDate, List<OfferDto> offers) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startPrice = startPrice;
        this.buyOutPrice = buyOutPrice;
        this.contactEmail = contactEmail;
        this.creationDate = creationDate;
        this.offers = offers;
    }

    public ItemDto(int id, String name, String description, BigInteger startPrice, BigInteger buyOutPrice, String contactEmail, Date creationDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startPrice = startPrice;
        this.buyOutPrice = buyOutPrice;
        this.contactEmail = contactEmail;
        this.creationDate = creationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigInteger getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigInteger startPrice) {
        this.startPrice = startPrice;
    }

    public BigInteger getBuyOutPrice() {
        return buyOutPrice;
    }

    public void setBuyOutPrice(BigInteger buyOutPrice) {
        this.buyOutPrice = buyOutPrice;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<OfferDto> getOffers() {
        return offers;
    }

    public void setOffers(List<OfferDto> offers) {
        this.offers = offers;
    }
}
