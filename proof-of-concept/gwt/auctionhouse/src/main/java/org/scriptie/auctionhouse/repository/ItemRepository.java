package org.scriptie.auctionhouse.repository;


import org.scriptie.auctionhouse.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("itemRepository")
public interface ItemRepository extends JpaRepository<Item, String> {
}
