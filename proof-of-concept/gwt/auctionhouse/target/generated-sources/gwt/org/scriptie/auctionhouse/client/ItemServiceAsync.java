package org.scriptie.auctionhouse.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ItemServiceAsync
{

    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see org.scriptie.auctionhouse.client.ItemService
     */
    void all( AsyncCallback<java.util.List<org.scriptie.auctionhouse.client.ItemDto>> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see org.scriptie.auctionhouse.client.ItemService
     */
    void create( java.lang.String name, java.lang.String description, java.math.BigInteger startPrice, java.math.BigInteger buyOutPrice, java.lang.String contactEmail, AsyncCallback<org.scriptie.auctionhouse.client.ItemDto> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see org.scriptie.auctionhouse.client.ItemService
     */
    void get( int id, AsyncCallback<org.scriptie.auctionhouse.client.ItemDto> callback );


    /**
     * Utility class to get the RPC Async interface from client-side code
     */
    public static final class Util 
    { 
        private static ItemServiceAsync instance;

        public static final ItemServiceAsync getInstance()
        {
            if ( instance == null )
            {
                instance = (ItemServiceAsync) GWT.create( ItemService.class );
            }
            return instance;
        }

        private Util()
        {
            // Utility class should not be instanciated
        }
    }
}
