package org.scriptie.auctionhouse.domain;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Item.class)
public abstract class Item_ {

	public static volatile SingularAttribute<Item, Integer> id;
	public static volatile SingularAttribute<Item, Date> creationDate;
	public static volatile SingularAttribute<Item, BigInteger> buyOutPrice;
	public static volatile SingularAttribute<Item, String> contactEmail;
	public static volatile SingularAttribute<Item, String> description;
	public static volatile ListAttribute<Item, Offer> offers;
	public static volatile SingularAttribute<Item, String> name;
	public static volatile SingularAttribute<Item, BigInteger> startPrice;

}

