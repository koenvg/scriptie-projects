package org.scriptie.auctionhouse.client;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ItemDto_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.math.BigInteger getBuyOutPrice(org.scriptie.auctionhouse.client.ItemDto instance) /*-{
    return instance.@org.scriptie.auctionhouse.client.ItemDto::buyOutPrice;
  }-*/;
  
  private static native void setBuyOutPrice(org.scriptie.auctionhouse.client.ItemDto instance, java.math.BigInteger value) 
  /*-{
    instance.@org.scriptie.auctionhouse.client.ItemDto::buyOutPrice = value;
  }-*/;
  
  private static native java.lang.String getContactEmail(org.scriptie.auctionhouse.client.ItemDto instance) /*-{
    return instance.@org.scriptie.auctionhouse.client.ItemDto::contactEmail;
  }-*/;
  
  private static native void setContactEmail(org.scriptie.auctionhouse.client.ItemDto instance, java.lang.String value) 
  /*-{
    instance.@org.scriptie.auctionhouse.client.ItemDto::contactEmail = value;
  }-*/;
  
  private static native java.util.Date getCreationDate(org.scriptie.auctionhouse.client.ItemDto instance) /*-{
    return instance.@org.scriptie.auctionhouse.client.ItemDto::creationDate;
  }-*/;
  
  private static native void setCreationDate(org.scriptie.auctionhouse.client.ItemDto instance, java.util.Date value) 
  /*-{
    instance.@org.scriptie.auctionhouse.client.ItemDto::creationDate = value;
  }-*/;
  
  private static native java.lang.String getDescription(org.scriptie.auctionhouse.client.ItemDto instance) /*-{
    return instance.@org.scriptie.auctionhouse.client.ItemDto::description;
  }-*/;
  
  private static native void setDescription(org.scriptie.auctionhouse.client.ItemDto instance, java.lang.String value) 
  /*-{
    instance.@org.scriptie.auctionhouse.client.ItemDto::description = value;
  }-*/;
  
  private static native int getId(org.scriptie.auctionhouse.client.ItemDto instance) /*-{
    return instance.@org.scriptie.auctionhouse.client.ItemDto::id;
  }-*/;
  
  private static native void setId(org.scriptie.auctionhouse.client.ItemDto instance, int value) 
  /*-{
    instance.@org.scriptie.auctionhouse.client.ItemDto::id = value;
  }-*/;
  
  private static native java.lang.String getName(org.scriptie.auctionhouse.client.ItemDto instance) /*-{
    return instance.@org.scriptie.auctionhouse.client.ItemDto::name;
  }-*/;
  
  private static native void setName(org.scriptie.auctionhouse.client.ItemDto instance, java.lang.String value) 
  /*-{
    instance.@org.scriptie.auctionhouse.client.ItemDto::name = value;
  }-*/;
  
  private static native java.util.List getOffers(org.scriptie.auctionhouse.client.ItemDto instance) /*-{
    return instance.@org.scriptie.auctionhouse.client.ItemDto::offers;
  }-*/;
  
  private static native void setOffers(org.scriptie.auctionhouse.client.ItemDto instance, java.util.List value) 
  /*-{
    instance.@org.scriptie.auctionhouse.client.ItemDto::offers = value;
  }-*/;
  
  private static native java.math.BigInteger getStartPrice(org.scriptie.auctionhouse.client.ItemDto instance) /*-{
    return instance.@org.scriptie.auctionhouse.client.ItemDto::startPrice;
  }-*/;
  
  private static native void setStartPrice(org.scriptie.auctionhouse.client.ItemDto instance, java.math.BigInteger value) 
  /*-{
    instance.@org.scriptie.auctionhouse.client.ItemDto::startPrice = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, org.scriptie.auctionhouse.client.ItemDto instance) throws SerializationException {
    setBuyOutPrice(instance, (java.math.BigInteger) streamReader.readObject());
    setContactEmail(instance, streamReader.readString());
    setCreationDate(instance, (java.util.Date) streamReader.readObject());
    setDescription(instance, streamReader.readString());
    setId(instance, streamReader.readInt());
    setName(instance, streamReader.readString());
    setOffers(instance, (java.util.List) streamReader.readObject());
    setStartPrice(instance, (java.math.BigInteger) streamReader.readObject());
    
  }
  
  public static org.scriptie.auctionhouse.client.ItemDto instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new org.scriptie.auctionhouse.client.ItemDto();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, org.scriptie.auctionhouse.client.ItemDto instance) throws SerializationException {
    streamWriter.writeObject(getBuyOutPrice(instance));
    streamWriter.writeString(getContactEmail(instance));
    streamWriter.writeObject(getCreationDate(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeInt(getId(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeObject(getOffers(instance));
    streamWriter.writeObject(getStartPrice(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return org.scriptie.auctionhouse.client.ItemDto_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    org.scriptie.auctionhouse.client.ItemDto_FieldSerializer.deserialize(reader, (org.scriptie.auctionhouse.client.ItemDto)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    org.scriptie.auctionhouse.client.ItemDto_FieldSerializer.serialize(writer, (org.scriptie.auctionhouse.client.ItemDto)object);
  }
  
}
