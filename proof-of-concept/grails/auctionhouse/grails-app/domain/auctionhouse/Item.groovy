package auctionhouse

class Item {
	String name
	String description
	BigDecimal startPrice
	BigDecimal buyOutPrice
	String contactEmail
	
	static hasmany = [offers: Offer]
}
