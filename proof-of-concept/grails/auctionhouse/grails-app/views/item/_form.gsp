<%@ page import="auctionhouse.Item" %>



<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'buyOutPrice', 'error')} required">
	<label for="buyOutPrice">
		<g:message code="item.buyOutPrice.label" default="Buy Out Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="buyOutPrice" value="${fieldValue(bean: itemInstance, field: 'buyOutPrice')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'contactEmail', 'error')} required">
	<label for="contactEmail">
		<g:message code="item.contactEmail.label" default="Contact Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="contactEmail" required="" value="${itemInstance?.contactEmail}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="item.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${itemInstance?.description}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="item.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${itemInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'startPrice', 'error')} required">
	<label for="startPrice">
		<g:message code="item.startPrice.label" default="Start Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="startPrice" value="${fieldValue(bean: itemInstance, field: 'startPrice')}" required=""/>

</div>

