package com.tutorial;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

/**
 * Created by koen on 26.05.14.
 */
public class HelloWorld extends WebPage {
    public HelloWorld() {
        add(new Label("message", "Hello world!"));
    }
}
