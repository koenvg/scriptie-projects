package hello;

/**
 * Created by koen on 05.04.14.
 */
public class Greeting {

    private final long id;
    private final String content;

    public Greeting(Long id, String content){
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
